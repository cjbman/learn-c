#include <stdio.h>

// declaring some functions here just to see the formatting (I think I will look at header files later)
int timesTen(int num);
int factorial(int num);
void timesAndFact(int num);

int main(){
    timesAndFact(6);
    return 0;
}

int timesTen(int num) {
    return num * 10;
}

int factorial(int num) {
    int fact = 1;
    for (int i = 1; i <= num; i++) {
        fact = fact * i;
    }
    return fact;
}

void timesAndFact(int num) {
    printf("%d x 10 = %d\n%d! = %d\n", num, timesTen(num), num, factorial(num));
}
